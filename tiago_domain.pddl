(define (domain tiago)

(:requirements :strips :typing :disjunctive-preconditions :durative-actions)

(:types
	waypoint 
	robot
	door
	room
)

(:predicates
	(robot_at ?v - robot ?wp - waypoint)
	(at_start ?v - robot)
	(visited ?r - room)
	(localised ?v - robot)
	(left_start ?v - robot)
	(is_closed ?d  - door)
	(is_open ?d  - door)
	(at_door ?v - robot ?d - door)
	(facing_door ?v - robot ?d - door)
	(in_room ?v - robot ?r - room)
	(outside ?v - robot)
	(connected_door ?d - door ?r - room)
	(connected_dooropen ?r - room)
)

;; Depart from start
(:durative-action leave_start
	:parameters (?v - robot)
	:duration ( = ?duration 5)
	:condition(at start (at_start ?v))
	:effect(and
		(at start (not (at_start ?v)))
		(at end (left_start ?v))
		)
)

;; Localise
(:durative-action localise
	:parameters (?v - robot)
	:duration ( = ?duration 20)
	:condition (at start (left_start ?v))
	:effect (at end (localised ?v))
)

;; Move to a door
(:durative-action goto_door
	:parameters (?from - waypoint ?to - door ?v - robot ?r -room)
	:duration ( = ?duration 15)
	:condition (and
		(at start (left_start ?v))
		(at start (robot_at ?v ?from))
		(at start (outside ?v))
		)
	:effect (and
		(at start (not (robot_at ?v ?from)))
		(at end (at_door ?v ?to))
		)
)

;; Face the door
(:durative-action face_door
	:parameters (?d - door ?v - robot)
	:duration ( = ?duration 10)
	:condition (at start (at_door ?v ?d))
	:effect (at end (facing_door ?v ?d))
)

;; Open door
(:durative-action open_door
	:parameters (?d - door ?v - robot ?r - room)
	:duration ( = ?duration 10)
	:condition (and
		  (at start (at_door ?v ?d))
		  (at start(facing_door ?v ?d))
		  (at start(is_closed ?d))
		  )
	:effect (and
		(at end (not (is_closed ?d)))
		(at end (is_open ?d))
		(at end (connected_dooropen ?r))
		)
)

;; Enter room
(:durative-action enter_room
	:parameters (?d - door ?v - robot ?r - room)
	:duration ( = ?duration 15)
	:condition (and
		  (at start (connected_door ?d ?r))
		  (at start (connected_dooropen ?r))
		  (at start (outside ?v))
		  (at start (facing_door ?v ?d))
		  (at start (is_open ?d))
		  )
	:effect (and
		(at start (not (facing_door ?v ?d)))
		(at end (not (at_door ?v ?d)))
		(at end (not (outside ?v)))
		(at end (in_room ?v ?r))
		(at end (visited ?r))
		)
)

;; Exit room
(:durative-action exit_room
	:parameters (?v - robot ?to - waypoint ?r - room)
	:duration ( = ?duration 15)
	:condition (at start (in_room ?v ?r))
	:effect (and
		(at end (not (in_room ?v ?r)))
		(at end (outside ?v))
		(at end (robot_at ?v ?to))
		)
)




)
