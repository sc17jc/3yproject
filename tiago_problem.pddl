(define (problem task)
(:domain tiago)
(:objects
    wp0 - waypoint
    r1 r2 r3 - room
    d1 d2 d3 - door
    tiago - robot
)
(:init
    (robot_at tiago wp0)
    (at_start tiago)
    (outside tiago)
    (is_closed d1)
    (is_closed d2)
    (is_closed d3)
    (connected_door d1 r1)
    (connected_door d2 r2)
    (connected_door d3 r3)


)
(:goal (and
    
    (visited r1)
    
    (visited r2)
    
    (visited r3)
)))
